package com.btu.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_update_profile.*

class UpdateProfileActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_profile)

        auth = FirebaseAuth.getInstance()
        val user = auth.currentUser

        confirmProfileUpdateButton.setOnClickListener {
            val database = Firebase.database
            val reference = database.getReference("users")
            val userRef = reference.child(user!!.uid)
            userRef.child("name").setValue(nameEditText.text.toString())
            userRef.child("surname").setValue(surnameEditText.text.toString())
            userRef.child("age").setValue(ageEditText.text.toString().toInt())
            val addressRef = userRef.child("address")
            addressRef.child("address_name").setValue(addressNameEditText.text.toString())
            addressRef.child("zip_code").setValue(zipCodeEditText.text.toString())
            addressRef.child("location").setValue(locationEditText.text.toString()).addOnCompleteListener {
                finish()
            }
        }
    }
}