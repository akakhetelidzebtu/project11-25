package com.btu.myapplication

class MessageModel(
    var message:String? = null,
    var name:String? = null,
    var time_stamp:Long? = null,
    var user_id:String? = null
)