package com.btu.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ServerValue
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        auth = FirebaseAuth.getInstance()
        val user = auth.currentUser

        val db = Firebase.database
        val chat = db.getReference("chat")
        chat.addValueEventListener(object: ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                val snapshots = snapshot.children
                var text = ""
                for(item in snapshots){
                    val message = item.getValue<MessageModel>()!!
                    text += "\n" + message.name + ": " + message.message
                }
                displayTextView.text = text
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })

        sendMessageButton.setOnClickListener {
            val ref = db.getReference("chat")
            val message = ref.push()
            message.child("message").setValue(chatMessageEditText.text.toString())
            message.child("user_id").setValue(user!!.uid)
            message.child("name").setValue(user.displayName)
            message.child("time_stamp").setValue(ServerValue.TIMESTAMP)
        }

        updateProfileButton.setOnClickListener {
            val intent = Intent(this,UpdateProfileActivity::class.java)
            startActivity(intent)
        }


        logoutButton.setOnClickListener {
            auth.signOut()
            val intent = Intent(this,MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }


    }
}